'''
Created on 12-Apr-2017

@author: gaurav
'''
import sys
import re
import operator
read = r"^read\(([a-zA-Z0-9]+)[\, ]+([a-zA-Z0-9]+)\)$"
write = r"^write\(([a-zA-Z0-9]+)[\, ]+([a-zA-Z0-9]+)\)$"
output = r"^output\(([a-zA-Z0-9]+)\)$"
exp = r"^([a-zA-Z0-9]+)(.)([a-zA-Z0-9]+)$"
ops = { "+": operator.add, "-": operator.sub , "*": operator.mul, "/": operator.div}
filename = sys.argv[1]
window = int(sys.argv[2])

f = open(filename)
lines = f.readlines()

# Initializing variables
key_vals = lines[0].split()
kv = {}
i = 0
while i+1 < len(key_vals):
    kv[key_vals[i]] = int(key_vals[i+1])
    i += 2
#print "Initial Values\n", kv

transactions = []
transaction_states = {}
i = 1
while i < len(lines):
    details = lines[i].split()
    details[1] = i + int(details[1])
    details.append(i+1)
    details.append(i+1)
    transactions.append(details)
    transaction_states[details[0]] = {}
    # transaction_name, line to stop, current line, start line
    i = details[1] + 1
#print "Transaction Details\n", transactions

def GenerateLog(lines):
    for l in lines:
        print l.strip()

# Performs operation and returns the value
def operation(op1, operator, op2):
    return ops[operator](op1,op2)

# Returns Integer Value of operand
def getVal(operand, state_dict):
    if not operand.isdigit():
        operand = state_dict[operand]
    return int(operand)

def toString(kv):
    res = ""
    for key in sorted(kv.keys()):
        res += key + " " + str(kv[key]) + " "
    return res

def update_kv(key, value):
    # 
    kv[key] = value

def Execute(transaction, transaction_state):
    # Execute a single transaction
    start = transaction[2]
    stop = transaction[2] + window
    if start == transaction[3]:
        # Push variable values and put start log
        transaction_state = kv.copy()
        statements = []
        statements.append("<" + transaction[0] + ", start>")
        statements.append(toString(kv))
        GenerateLog(statements)
    while start <= transaction[1] and start < stop:
        # re.search(read, lines[start])re.compile(read).match(lines[start])
        if re.search(read, lines[start]):
            match = re.search(read, lines[start])
            transaction_state[match.group(1)] = kv[match.group(1)]
            transaction_state[match.group(2)] = transaction_state[match.group(1)]
            #statements = []
            #GenerateLog(statements)
            #print "Read Executed"
        elif re.search(write, lines[start]):
            match = re.search(write, lines[start])
            statements = []
            statements.append("<" + transaction[0] + ", " + match.group(1) + ", "  + str(kv[match.group(1)]) + ">")
            transaction_state[match.group(1)] = transaction_state[match.group(2)]
            update_kv(match.group(1), transaction_state[match.group(2)])
            statements.append(toString(kv))
            GenerateLog(statements)
            #print "Write Executed"
        elif re.search(output, lines[start]):
            match = re.search(output, lines[start])
            #print "Value written to disk"
            statements = []
            statements.append(toString(kv))
            # GenerateLog(statements)
        else:
            temp = lines[start].split()
            match = re.search(exp, temp[2])
            op1, op, op2 = match.group(1), match.group(2), match.group(3)
            transaction_state[temp[0]] = operation(getVal(op1, transaction_state), op, getVal(op2, transaction_state))
            #print "Operation Performed"
        start = start + 1
    if start > transaction[1]:
        #Transaction Ended
        statements = []
        statements.append("<" + transaction[0] + ", commit>")
        statements.append(toString(kv))
        GenerateLog(statements)
    return start, transaction_state

def controller():
    completed = {}
    while len(completed) < len(transactions):
        for transaction in transactions:
            if transaction[2] <= transaction[1]:
                #print transaction[0], "in Execution"
                transaction[2], transaction_states[transaction[0]] = Execute(transaction, transaction_states[transaction[0]])
            else:
                completed[transaction[0]] = True

controller()