import sys
import re
import operator

start = r"^\<([a-zA-Z0-9]+)[\, ]+start\>$"
commit = r"^\<([a-zA-Z0-9]+)[\, ]+commit\>$"
start_ckpt = r"^\<start ckpt[\, ]+([a-zA-Z0-9\, ]+)\>$"
val_read = r"^\<([a-zA-Z0-9]+)[\, ]+([a-zA-Z0-9]+)[\, ]+([a-zA-Z0-9]+)\>$"
end_ckpt = r"^\<end ckpt\>$"

filename = sys.argv[1]
f = open(filename)
lines = f.readlines()

# Initializing variables
key_vals = lines[0].split()

kv = {}
recoverkv = {}
i = 0
while i+1 < len(key_vals):
    kv[key_vals[i]] = int(key_vals[i+1])
    i += 2

transaction_states = {}

end_ckpt_flag = False
i = len(lines) - 1
while i >= 1:
	if re.search(start, lines[i]):
		# 
		match = re.search(start, lines[i])
	elif re.search(commit, lines[i]):
		# 
		match = re.search(commit, lines[i])
		transaction_states[match.group(1)] = True
	elif re.search(start_ckpt, lines[i]):
		# 
		if end_ckpt_flag:
			break
		match = re.search(start_ckpt, lines[i])
	elif re.search(end_ckpt, lines[i]):
		# 
		end_ckpt_flag = True
	elif re.search(val_read, lines[i]):
		# 
		match = re.search(val_read, lines[i])
		t_name, var, val = match.group(1), match.group(2), match.group(3)
		if transaction_states.has_key(t_name):
			if not transaction_states[t_name]:
				recoverkv[var] = val
		else:
			transaction_states[t_name] = False
			recoverkv[var] = val
	i -= 1

res = ""
for key in sorted(recoverkv.keys()):
    res += key + " " + str(recoverkv[key]) + " "
print res.strip()